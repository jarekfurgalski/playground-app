import React, {Component, useContext} from 'react';
import {Route, Redirect} from 'react-router-dom';
// import { AuthenticationContext } from "../context/context";

// tslint:disable-next-line
const PrivateRoute = ({component: Component, ...rest}: any) => {
  // const { authenticated } = useContext(AuthenticationContext);

  // You should set your authentication properly!
  const authenticated = true;

  return (
    <Route
      {...rest}
      render={(props) =>
        authenticated ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};

export default PrivateRoute;
