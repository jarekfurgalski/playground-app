import React, { Component, Fragment } from 'react';
import { HomeProps }  from './home.props';
import { HomeState } from './home.state';
import './Home.styles.scss';

export class HomeScreen extends Component<HomeProps, HomeState>{

    // constructor(props: HomeProps){
    //     super(props);    
    // }

    componentDidMount(){
        console.log('Component did mount'); 
    }

    componentWillUnmount(){
        console.log('Component will unmount');
    }

    render(){
        
        return(
            <Fragment>
                <h1>Home</h1>
            </Fragment>
        )
    }
}