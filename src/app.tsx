import React from 'react';
import AppRouter from './routes/approuter';
import './styles/app.scss';

function App() {
  return (
    <AppRouter />
  );
}

export default App;